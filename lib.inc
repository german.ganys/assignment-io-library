section .text

%define SYSCALL_READ 0
%define SYSCALL_WRITE 1
%define SYSCALL_EXIT 60

%define STDIN 0
%define STDOUT 1
%define STDERR 2


exit:
    mov rax, SYSCALL_EXIT
    xor rdi, rdi
    syscall
    ret

;input:
;	rdi: str address
;output:
;	rax: len
string_length:
    xor rax, rax
	.next:
		cmp byte[rdi+rax], 0
		je .found
		inc rax
		jmp .next
.found:
	ret

print_string_err:
    push rdi
    call string_length
    pop rsi

    mov rdx, rax
    mov rax, SYSCALL_WRITE
    mov rdi, STDERR
    syscall
    xor rax, rax
    ret

print_string:
    push rdi
	call string_length
	pop rsi

	mov rdx, rax
	mov rax, SYSCALL_WRITE
	mov rdi, STDOUT
	syscall
	xor rax, rax
	ret

print_newline:
	mov rdi, `\n`


;input:
;	rdi: ascii code
;output:
;	print(char)

print_char:
    push rdi
    mov rax, SYSCALL_WRITE
    mov rdx, 1
    mov rdi, STDOUT
    mov rsi, rsp
    syscall
    pop rdi
    ret



;input:
;	rax: int
;out:
;	print(int)
print_uint:
	mov r9, rsp ; save rsp
	mov rax, rdi
	mov rcx, 10 ; modulo
	dec rsp;
	mov byte[rsp], 0
    .loop:
	    mov rdx, 0 ; mod to 0
	    div rcx
	    add rdx, '0'
	    dec rsp
	    mov byte[rsp], dl
	    test rax, rax ; ZF
	        jnz .loop

	mov rdi, rsp
	push r9
	call print_string
	pop r9
	mov rsp, r9
	ret



;input:
;	rdi - int
;out:
;	print(int)
print_int:
	cmp rdi, 0
        jge .uint

	push rdi
	mov rdi, "-"
	call print_char
	pop rdi
	neg rdi
.uint:
	call print_uint
	ret

;input:
;	rdi - str 1 address
;	rsi - str 2 address
;output:
;	rax: eq or not (1|0)
string_equals:
	xor rax, rax
	.loop:
		mov r8b, byte[rdi+rax]
		mov r9b, byte[rsi+rax]
		cmp r8b, r9b
		    jne .differ
		cmp r8b, 0
		    je .same
		inc rax
		jmp .loop
	.same:
		xor rax, rax
		mov rax, 1
		ret
	.differ:
		xor rax, rax
		mov rax, 0
		ret

read_char:
	mov rax, SYSCALL_READ
	mov rdi, STDIN
	mov rdx, 1 ; reads 1 symbol
	push 0
	mov rsi, rsp
	syscall
	pop rax
	ret

;input:
;	rdi - buffer string address
;	rsi - buffer string size
;output:
;	rax:	Filled buffer link
;		0 if operation failed
;	rdx: Filled buffer size
read_word:
	xor rcx, rcx ; offset from buffer (rdi)

.del_space:
    push rdi
    push rcx
	call read_char
	pop rcx
	pop rdi

    cmp rax, 0x0
	    je .end

    cmp rax, `\n`
	    je .del_space

	cmp rax, ' '
	    je .del_space

	cmp rax, `\t`
	    je .del_space


.next_char:
	cmp rsi, rcx
	    jbe .ov
	mov byte[rdi + rcx], al

	cmp al, 0x0
	    je .end

	cmp al, ' '
	    je .end

	cmp al, `\n`
	    je .end

	cmp al, `\t`
	    je .end

	inc rcx

	cmp rcx, rsi
	    je .ov

    push rdi
    push rcx
	call read_char
	pop rcx
	pop rdi

	jmp .next_char

.ov:
	mov rax, 0
	ret
.end:
	mov byte[rdi + rcx], 0
	mov rax, rdi
	mov rdx, rcx
	ret

;input:
;   rdi: String link
;out:
;	rax - number
;	rdx - number length
;	0 - failed
parse_uint:
	xor r10, r10 ; string offset (num len)
	mov rcx, 10 ; number base
	mov rax, 0 ; target
	.loop:
		xor r11, r11
		mov r11b, byte[rdi + r10]

		cmp r11b, '9'
		    ja .end
		cmp r11b, '0'
		    jb .end

		inc r10
		sub r11b, '0'
		mul rcx
		add rax, r11
		jmp .loop
	.end:
		mov rdx, r10
		ret

;input:
;	rdi - str address
;output:
;   rax: Number
;	rdx: Number length (0 if operation failed)

parse_int:
	cmp byte[rdi], '-'
	    jne .uint
	inc rdi

	call parse_uint

	cmp rdx, 0
	    je .not_digit

	neg rax
	inc rdx
	ret
.uint:
	call parse_uint
	cmp rdx, 0
        je .not_digit
	ret

.not_digit:
	xor rax, rax
	ret

;input:
;	rdi - string address source
;	rsi - target buffer string address
;	rdx - target buffer string size
;out:
;	rax - len

string_copy:
    push rsi
    push rdi

    call string_length

    pop rdi
    pop rsi

    inc rax ; adds zero byte to len
    cmp rdx, rax

    jb .error
    xor rax, rax
    .next:
        mov dl, byte[rdi + rax]
        mov byte[rsi + rax], dl
        inc rax
        cmp dl, 0
        jne .next
    ret
    .error:
        xor rax, rax
        ret
